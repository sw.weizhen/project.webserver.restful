package registrar

import (
	"net/http"

	"gitlab.com/sw.weizhen/project.webserver.restful/modules/userinfo"
)

type ServInfo struct {
	Version  string
	Host     string
	RemoteIP string
	Method   string
	User     *userinfo.UserInfo
}

type RESTController struct {
	Method   uint8
	URI      string
	Callback func(paramReq *ParamReq, servInfo *ServInfo) []byte

	Optional Option
}

type Option struct {
	VerifyAuth                bool
	BodyContent               bool
	CustomizedHandle          bool
	ContentType               string
	VerifyPrivilegeActionCode string
}

type Router struct {
	APIs []RESTController
}

type ParamReq struct {
	RESTMap     map[string]string
	URIParam    map[string][]string
	FormParam   map[string][]string
	BodyContent string

	Resp http.ResponseWriter
	Req  *http.Request
}

package registrar

func (ref *Router) AddRouter(method uint8, uri string, callback func(paramReq *ParamReq, servInfo *ServInfo) []byte, option ...Option) {

	var opt Option
	if len(option) >= 1 {
		opt = option[0]
	}

	var api RESTController = RESTController{
		Method:   method,
		URI:      uri,
		Callback: callback,
		Optional: opt,
	}

	ref.APIs = append(ref.APIs, api)
}

func (ref *Router) GetRouter() []RESTController {
	return ref.APIs
}

package openvar

import (
	swmongo "gitlab.com/sw.weizhen/nosql.mongo"
	swredis "gitlab.com/sw.weizhen/nosql.redis"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
	rotatelogs "gitlab.com/sw.weizhen/util.logger"
)

func init() {
	WhiteList = make([]string, 0)

	DB = &DBContainer{
		dbls: make(map[string]*swmysql.DBOperator),
	}

}

var Log *rotatelogs.RotateLog
var Mongo *swmongo.MongoOperator
var Cache *swredis.SWRedis
var DB *DBContainer

var WhiteList []string

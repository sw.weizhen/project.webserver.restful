package openvar

import (
	"sync"

	swredis "gitlab.com/sw.weizhen/nosql.redis"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
)

type CacheContainer struct {
	redisls map[string]*swredis.SWRedis
	rwx     sync.RWMutex
}

func (ref *CacheContainer) Put(redisSite string, cache *swredis.SWRedis) {
	ref.rwx.Lock()
	ref.redisls[redisSite] = cache
	ref.rwx.Unlock()
}

func (ref *CacheContainer) Get(redisSite string) *swredis.SWRedis {
	var cache *swredis.SWRedis

	ref.rwx.RLock()
	cache = ref.redisls[redisSite]
	ref.rwx.RUnlock()

	return cache
}

type DBContainer struct {
	dbls map[string]*swmysql.DBOperator
	rwx  sync.RWMutex
}

func (ref *DBContainer) Put(dbName string, db *swmysql.DBOperator) {
	ref.rwx.Lock()
	ref.dbls[dbName] = db
	ref.rwx.Unlock()
}

func (ref *DBContainer) Get(dbName string) *swmysql.DBOperator {
	var db *swmysql.DBOperator

	ref.rwx.RLock()
	db = ref.dbls[dbName]
	ref.rwx.RUnlock()

	return db
}

package server

import (
	"errors"
	"fmt"
	"log"
	"strings"

	swmongo "gitlab.com/sw.weizhen/nosql.mongo"
	swredis "gitlab.com/sw.weizhen/nosql.redis"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
	rotatelogs "gitlab.com/sw.weizhen/util.logger"

	"gitlab.com/sw.weizhen/project.webserver.restful/modules/cfgreader"
	"gitlab.com/sw.weizhen/project.webserver.restful/openvar"
)

func loadServerConfig(serverConfigPath string) error {

	ftserverConfig = websiteCfg{}
	err := cfgreader.ReadYAMLConfig(serverConfigPath, &ftserverConfig)
	if err != nil {
		return err
	}

	whiteLs, ok := ftserverConfig.Manage["whitelist"]
	if ok {
		for _, val := range whiteLs {
			openvar.WhiteList = append(openvar.WhiteList, val.IP)
		}
	}

	return nil
}

func deployLogger() error {
	system := ftserverConfig.LogCfg.Sys
	sysVal := rotatelogs.SYS_WINDOWS
	if strings.ToLower(system) == "linux" {
		sysVal = rotatelogs.SYS_LINUX
	}

	mode := ftserverConfig.LogCfg.Mode
	modVal := rotatelogs.MODE_DEFAULT
	if strings.ToLower(mode) == "debug" {
		modVal = rotatelogs.MODE_DEBUG
	} else if strings.ToLower(mode) == "default" {
		modVal = rotatelogs.MODE_DEFAULT
	} else {
		modVal = rotatelogs.MODE_SYSTEM
	}

	size := ftserverConfig.LogCfg.Maxsize
	if size <= 0 {
		size = 50
	}

	workpath := ftserverConfig.LogCfg.Workpath
	if !strings.HasSuffix(workpath, "/") {
		workpath += "/"
	}

	var err error
	openvar.Log, err = rotatelogs.New(sysVal, modVal, size, workpath)
	if err != nil {
		return errors.New(err.Error())
	}

	log.Printf("[LOGGER] mode: %s", mode)
	log.Printf("[LOGGER] max size: %dmb", size)
	log.Print("[LOGGER] logger activated")

	return nil
}

func deployDB() error {

	dbLs := ftserverConfig.RelationalDB["mysql"]
	for i := 0; i < len(dbLs); i++ {
		dbCfg := dbLs[i]
		database := dbCfg.Database
		host := dbCfg.Host
		port := dbCfg.Port
		user := dbCfg.User
		password := dbCfg.Password
		charset := dbCfg.Charset
		maxIdleConns := dbCfg.MaxIdleConns
		maxOpenConns := dbCfg.MaxOpenConns
		// interpolateParams := dbCfg.InterpolateParams
		// multiStatements := dbCfg.MultiStatements
		enable := dbCfg.Enable

		if enable {
			if len(database) == 0 {
				return errors.New("undefined database name")
			}

			log.Printf("[DB %d] connect to db: %s, host: %s, port: %d", i, database, host, port)
			dbPtr, err := swmysql.New(
				user,
				password,
				host,
				port,
				database,
				charset,
				maxIdleConns,
				maxOpenConns,
				// interpolateParams,
				// multiStatements,
			)

			if err != nil {
				return fmt.Errorf("%s connection failed: %v", database, err)
			}

			if dbPtr == nil {
				return fmt.Errorf("fail to new object(%s), nil point", database)
			}

			dbPtr.SetMaxIdleConns(maxIdleConns)
			dbPtr.SetMaxOpenConns(maxOpenConns)

			if ok := dbPtr.CheckDBStatus(); !ok {
				return fmt.Errorf("check db(%s:%d/%s) status failed", host, port, database)
			}

			openvar.DB.Put(database, dbPtr)

			log.Printf("[DB %d] address: %s:%d", i, host, port)
			log.Printf("[DB %d] charset: %s", i, charset)
			log.Printf("[DB %d] max idle connections: %d", i, maxIdleConns)
			log.Printf("[DB %d] max open connections: %d", i, maxOpenConns)
			// log.Printf("[DB %d] interpolateParams: %t", i, interpolateParams)
			// log.Printf("[DB %d] multiStatements: %t", i, multiStatements)
			log.Printf("[DB %d] %s activated", i, database)
		}
	}

	return nil
}

func deployCache() error {

	redisCfgls := ftserverConfig.Cache["redis"]

	if len(redisCfgls) == 0 {
		return nil
	}

	redisCfg := redisCfgls[0]
	if redisCfg.Enable {
		host := redisCfg.Host
		port := redisCfg.Port

		log.Printf("[CACHE] connect to redis, host: %s, port: %d", host, port)

		addr := fmt.Sprintf("%s:%d", host, port)

		cachePtr, err := swredis.New(addr, "")
		if err != nil {
			return fmt.Errorf("redis connection failed: %v", err)
		}

		if cachePtr == nil {
			return errors.New("fail to new redis object, nil point")
		}

		openvar.Cache = cachePtr

		log.Printf("[CACHE] address: %s:%d", host, port)
		log.Print("[CACHE] redis activated !")
	}

	return nil
}

func deployMongo() error {

	mongoCfgLs := ftserverConfig.NoSQL["mongo"]
	if len(mongoCfgLs) == 0 {
		return nil
	}

	mongoCfg := mongoCfgLs[0]
	if mongoCfg.Enable {

		host := mongoCfg.Host
		port := mongoCfg.Port

		log.Printf("[MONGO] connect to mongo, host: %s, port: %d", host, port)

		mongoPtr, err := swmongo.New(host, port)
		if err != nil {
			return err
		}

		if mongoPtr == nil {
			return errors.New("fail to new mongo object, nil point")
		}

		if ok := mongoPtr.IsAlive(); !ok {
			return fmt.Errorf("check mongo(%s:%d) status failed", host, port)
		}

		openvar.Mongo = mongoPtr

		log.Printf("[MONGO] address: %s:%d", host, port)
		log.Print("[MONGO] mongo activated")

	}

	return nil
}

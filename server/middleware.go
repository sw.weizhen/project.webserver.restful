package server

import (
	"time"

	r "gitlab.com/sw.weizhen/project.webserver.restful/modules/responsedata"
	j "gitlab.com/sw.weizhen/project.webserver.restful/modules/swjwt"
	u "gitlab.com/sw.weizhen/project.webserver.restful/modules/userinfo"
	o "gitlab.com/sw.weizhen/project.webserver.restful/openvar"
)

func NewJWToken(user u.UserInfo) (string, error) {

	key := []byte(ftserverConfig.Serv.CryptoKey)
	user.ExpirationTime = time.Now().Add(time.Minute * time.Duration(ftserverConfig.Serv.TokenExpireTime)).Unix()

	token := j.New(j.SigningHS256, user)
	tk, err := token.Sign(key)
	if err != nil {
		return EMPTY, err
	}

	return tk, nil
}

func verifyAuthToken(token string) (*u.UserInfo, string, int, string) {

	key := []byte(ftserverConfig.Serv.CryptoKey)
	tk, err := j.Decrypt(token, &u.UserInfo{}, func(token *j.JWToken) (interface{}, error) {
		return []byte(key), nil
	})

	// if err != nil {
	// 	openvar.Log.ServerError(err.Error())
	// 	return nil, j.EMPTY, r.ERROR_CODE_DECRYPT_FAIL, r.ERROR_MSG_DECRYPT_FAIL
	// }

	if tk == nil {
		return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_NIL_TOKEN, r.ERROR_MSG_ACCOUNT_AUTH_NIL_TOKEN
	}

	if tk.Valid {
		usrInf, ok := tk.Claims.(*u.UserInfo)
		if !ok {
			return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_CLAIMS, r.ERROR_MSG_ACCOUNT_AUTH_CLAIMS
		}

		usrInf.ExpirationTime = time.Now().Add(time.Minute * time.Duration(ftserverConfig.Serv.TokenExpireTime)).Unix()

		newTK := j.New(j.SigningHS256, usrInf)
		signature, err := newTK.Sign([]byte(key))
		if err != nil {
			o.Log.ServerError(err.Error())
			return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_TOKEN_REFRESH, r.ERROR_MSG_ACCOUNT_AUTH_TOKEN_REFRESH
		}

		return usrInf, signature, r.ERROR_CODE_NONE, r.ERROR_MSG_NONE

	} else if e, ok := err.(*j.ErrorValidation); ok {
		if e.Errors&j.ERR_VALIDATION_MALFORMED != 0 {
			return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_NOT_TOKEN, r.ERROR_MSG_ACCOUNT_AUTH_NOT_TOKEN

		} else if e.Errors&(j.ERR_VALIDATION_NOT_VALID_YET|j.ERR_VALIDATION_EXPIRED) != 0 {
			return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_EXPIRED_TOKEN, r.ERROR_MSG_ACCOUNT_AUTH_EXPIRED_TOKEN

		} else {
			return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_ILLEGAL_TOKEN, r.ERROR_MSG_ACCOUNT_AUTH_ILLEGAL_TOKEN
		}
	}

	return nil, j.EMPTY, r.ERROR_CODE_ACCOUNT_AUTH_ILLEGAL_TOKEN, r.ERROR_MSG_ACCOUNT_AUTH_ILLEGAL_TOKEN
}

// import (
// 	"encoding/json"
// 	"fmt"

// 	"gitlab.com/alexia.shaowei/submodule/commonutil"
// 	"gitlab.com/alexia.shaowei/sw.webframe.shell/openvar"
// )

// type UserInfo struct {
// 	Uid      int    `json:"id"`
// 	Username string `json:"username"`
// 	RemoteIP string `json:"remoteip"`
// }

// func SaveUserToken(id int, account string, password string, remoteAddr string) (string, error) {
// 	token := commonutil.SHA256AccountToken(id, account, password)

// 	var userDetail UserInfo = UserInfo{id, account, remoteAddr}

// 	jsonUserDetail, err := json.Marshal(userDetail)
// 	if err != nil {
// 		return "", err
// 	}

// 	err = openvar.Cache.FormatSet(token, string(jsonUserDetail), 5*60)
// 	if err != nil {
// 		return "", err
// 	}

// 	return token, nil
// }

// func SearchUserToken(token string) (UserInfo, error) {

// 	val := openvar.Cache.FormatGet(token)
// 	if val == nil {
// 		return UserInfo{}, fmt.Errorf("token not exist")
// 	}

// 	err := openvar.Cache.FormatSet(token, val.Data, 5*60)
// 	if err != nil {
// 		return UserInfo{}, fmt.Errorf("fail to update token expire time")
// 	}

// 	var userInfo *UserInfo
// 	err = json.Unmarshal([]byte(val.Data), &userInfo)
// 	if err != nil {
// 		return UserInfo{}, fmt.Errorf("fail to unmarshal user data")
// 	}

// 	return *userInfo, nil
// }

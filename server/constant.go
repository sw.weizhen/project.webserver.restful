package server

const (
	CONFIG_PATH = "server.yml"
)

const (
	EMPTY                        = ""
	STAR                         = "*"
	CONTENT_TYPE                 = "Content-Type"
	ACCESS_CONTROL_ALLOW_ORIGIN  = "Access-Control-Allow-Origin"
	ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers"
)

const (
	PARAM_JWT_TOKEN  = "tk"
	PARAM_JWT_HEADER = "Authorization"
)

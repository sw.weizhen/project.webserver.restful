package server

import (
	"strings"
)

func removeAddrPort(addr string) string {
	seg := strings.Split(addr, ":")
	return seg[0]
}

package server

import (
	"log"

	core "gitlab.com/sw.weizhen/project.webserver.mux"
)

func Deploy(cfgPath ...string) *WebServer {

	path := CONFIG_PATH
	if len(cfgPath) > 0 {
		path = cfgPath[0]
	}

	if len(path) == 0 {
		path = CONFIG_PATH
	}

	if err := loadServerConfig(path); err != nil {
		log.Fatal(err)
	}

	if err := deployLogger(); err != nil {
		log.Fatal(err)
	}

	if err := deployDB(); err != nil {
		log.Fatal(err)
	}

	if err := deployCache(); err != nil {
		log.Fatal(err)
	}

	if err := deployMongo(); err != nil {
		log.Fatal(err)
	}

	return &WebServer{
		Ws:         core.SpreadRouter(),
		serverConf: ftserverConfig,
	}
}

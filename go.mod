module gitlab.com/sw.weizhen/project.webserver.restful

go 1.16

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	gitlab.com/sw.weizhen/nosql.mongo v0.0.2
	gitlab.com/sw.weizhen/nosql.redis v0.0.2
	gitlab.com/sw.weizhen/project.webserver.mux v0.0.0
	gitlab.com/sw.weizhen/rdbms.mysql v0.0.7
	gitlab.com/sw.weizhen/util.logger v0.0.0
)

package cfgreader

import (
	"io/ioutil"
	"strings"
)

func loadFile(serverConfig *ServerConfig) error {
	// defer recoverLoadFileError()

	data, err := ioutil.ReadFile(serverConfig.filePath)
	if err != nil {
		return err
	}

	dataArray := strings.Split(string(data), "\n")

	var section string

	isComment := false
	for _, oneline := range dataArray {
		oneline = strings.TrimSpace(oneline)
		oneline = strings.TrimPrefix(oneline, "\n")

		if len(oneline) < 2 {
			continue
		}

		if strings.HasPrefix(oneline, "#") || strings.HasPrefix(oneline, "//") {
			continue
		}

		if isComment {
			if strings.HasPrefix(oneline, "*/") {
				isComment = false
			}
			continue
		}

		if strings.HasPrefix(oneline, "/*") {
			isComment = true
			continue
		}

		if strings.HasPrefix(oneline, "[") && strings.HasSuffix(oneline, "]") {
			section = strings.TrimPrefix(strings.TrimSuffix(oneline, "]"), "[")
			serverConfig.config[section] = make(sectionConf)

		} else {
			valueArr := strings.SplitN(oneline, "=", 2)
			if len(valueArr) != 2 {
				continue
			}
			serverConfig.config[section][strings.TrimSpace(valueArr[0])] = strings.TrimSpace(valueArr[1])
		}
	}

	return err
}

// func recoverLoadFileError() {
// 	if err := recover(); err != nil {

// 	}
// }

package cfgreader

import (
	"io/ioutil"
	"strconv"
	"time"

	yml "github.com/go-yaml/yaml"
)

func ReadYAMLConfig(serverConfigPath string, ymlData interface{}) error {
	ymlF, err := ioutil.ReadFile(serverConfigPath)
	if err != nil {
		return err
	}

	err = yml.Unmarshal(ymlF, ymlData)
	if err != nil {
		return err
	}

	return nil
}

func ReadConfig(serverConfigPath string, autoReloadDuration time.Duration) (*ServerConfig, error) {
	serverConfig := ServerConfig{
		config:     make(map[string]sectionConf),
		filePath:   serverConfigPath,
		autoReload: autoReloadDuration,
	}

	err := loadFile(&serverConfig)
	if err != nil {
		return nil, err
	}

	return &serverConfig, nil
}

func (ref *ServerConfig) GetServConfString(section string, key string) (string, bool) {

	var isExist bool
	var rtnValue string

	if len(ref.config[section]) > 0 {
		if len(ref.config[section][key]) > 0 {
			isExist = true
			rtnValue = ref.config[section][key]
		}
	}

	return rtnValue, isExist
}

func (ref *ServerConfig) GetServConfUint32(section string, key string) (uint32, bool) {

	var isExist bool
	var rtnValue uint32

	if len(ref.config[section]) > 0 {
		if len(ref.config[section][key]) > 0 {
			value, err := strconv.ParseUint(ref.config[section][key], 0, 32)
			if err == nil {
				rtnValue = uint32(value)
				isExist = true
			}
		}
	}

	return rtnValue, isExist
}

func (ref *ServerConfig) GetServConfInt(section string, key string) (int, bool) {

	var isExist bool
	var rtnValue int

	if len(ref.config[section]) > 0 {
		if len(ref.config[section][key]) > 0 {
			value, err := strconv.ParseUint(ref.config[section][key], 0, 32)
			if err == nil {
				rtnValue = int(value)
				isExist = true
			}
		}
	}

	return rtnValue, isExist
}

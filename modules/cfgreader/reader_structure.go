package cfgreader

import (
	"time"
)

type sectionConf map[string]string

type ServerConfig struct {
	config     map[string]sectionConf
	filePath   string
	autoReload time.Duration
	// rwLock     sync.RWMutex
}

package main

type websiteCfg struct {
	Serv         servCfg               `yaml:"server"`
	LogCfg       logCfg                `yaml:"log"`
	RelationalDB map[string][]mysqlCfg `yaml:"rdb"`
	Cache        map[string][]redisCfg `yaml:"cache"`
	NoSQL        map[string][]mongoCfg `yaml:"nosql"`
}

type servCfg struct {
	Website string `yaml:"website"`
	System  string `yaml:"system"`
	Port    int    `yaml:"port"`
}

type mysqlCfg struct {
	Database          string `yaml:"database"`
	Host              string `yaml:"host"`
	Port              int    `yaml:"port"`
	User              string `yaml:"user"`
	Password          string `yaml:"password"`
	Charset           string `yaml:"charset"`
	MaxIdleConns      int    `yaml:"maxIdleConns"`
	MaxOpenConns      int    `yaml:"maxOpenConns"`
	InterpolateParams bool   `yaml:"interpolateParams"`
	MultiStatements   bool   `yaml:"multiStatements"`
	Enable            bool   `yaml:"enable"`
}

type redisCfg struct {
	Host   string `yaml:"host"`
	Port   int    `yaml:"port"`
	Enable bool   `yaml:"enable"`
}

type mongoCfg struct {
	Host   string `yaml:"host"`
	Port   int    `yaml:"port"`
	Enable bool   `yaml:"enable"`
}

type logCfg struct {
	Workpath string `yaml:"workpath"`
	System   string `yaml:"system"`
	Mode     string `yaml:"mode"`
	Maxsize  int    `yaml:"maxsize"`
}

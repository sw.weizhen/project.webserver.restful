package main

import (
	// "gitlab.com/alexia.shaowei/submodule/cfgreader"
	"gitlab.com/sw.weizhen/project.webserver.restful/modules/cfgreader"
)

var ftserverConfig FTServerConfig

type FTServerConfig struct {
	ServerFilePath string

	LocalPort uint32

	DB             string
	DBHost         string
	DBPort         int
	DBUser         string
	DBPassword     string
	DBMaxIdleConns int
	DBSwitch       int

	Cache       string
	CacheHost   string
	CachePort   int
	CacheSwitch int

	MongoHost   string
	MongoPort   int
	MongoSwitch int

	AWSAccessKey    string
	AWSSecrectKey   string
	AWSBucketName   string
	AWSImagesDomain string

	StoragePath string

	LogPath    string
	LogMode    string
	LogSystem  string
	LogMaxSize int

	IconPath string
}

func loadServerConfig(serverConfigPath string) error {
	serverConfig, err := cfgreader.ReadConfig(serverConfigPath, 0)
	if err != nil {
		return err
	}

	if filepath, ok := serverConfig.GetServConfString("SERVER", "filepath"); ok {
		ftserverConfig.ServerFilePath = filepath
	}

	if localport, ok := serverConfig.GetServConfUint32("SERVER", "localport"); ok {
		ftserverConfig.LocalPort = localport
	}

	if db, ok := serverConfig.GetServConfString("DATABASE", "db"); ok {
		ftserverConfig.DB = db
	}

	if dbhost, ok := serverConfig.GetServConfString("DATABASE", "dbhost"); ok {
		ftserverConfig.DBHost = dbhost
	}

	if dbport, ok := serverConfig.GetServConfInt("DATABASE", "dbport"); ok {
		ftserverConfig.DBPort = dbport
	}

	if dbuser, ok := serverConfig.GetServConfString("DATABASE", "dbuser"); ok {
		ftserverConfig.DBUser = dbuser
	}

	if dbpassword, ok := serverConfig.GetServConfString("DATABASE", "dbpassword"); ok {
		ftserverConfig.DBPassword = dbpassword
	}

	if dbMaxIdleConns, ok := serverConfig.GetServConfInt("DATABASE", "dbMaxIdleConns"); ok {
		ftserverConfig.DBMaxIdleConns = int(dbMaxIdleConns)
	}

	if dbSwitch, ok := serverConfig.GetServConfInt("DATABASE", "switch"); ok {
		ftserverConfig.DBSwitch = int(dbSwitch)
	}

	if logPath, ok := serverConfig.GetServConfString("LOG", "workpath"); ok {
		ftserverConfig.LogPath = logPath
	}

	if logMode, ok := serverConfig.GetServConfString("LOG", "mode"); ok {
		ftserverConfig.LogMode = logMode
	}

	if logSystem, ok := serverConfig.GetServConfString("LOG", "system"); ok {
		ftserverConfig.LogSystem = logSystem
	}

	if logMaxSize, ok := serverConfig.GetServConfInt("LOG", "maxsize"); ok {
		ftserverConfig.LogMaxSize = logMaxSize
	}

	if cache, ok := serverConfig.GetServConfString("CACHE", "cache"); ok {
		ftserverConfig.Cache = cache
	}

	if cachehost, ok := serverConfig.GetServConfString("CACHE", "cachehost"); ok {
		ftserverConfig.CacheHost = cachehost
	}

	if cacheport, ok := serverConfig.GetServConfInt("CACHE", "cacheport"); ok {
		ftserverConfig.CachePort = cacheport
	}

	if cacheSwitch, ok := serverConfig.GetServConfInt("CACHE", "switch"); ok {
		ftserverConfig.CacheSwitch = cacheSwitch
	}

	if mongohost, ok := serverConfig.GetServConfString("MONGO", "mongohost"); ok {
		ftserverConfig.MongoHost = mongohost
	}

	if mongoport, ok := serverConfig.GetServConfInt("MONGO", "mongoport"); ok {
		ftserverConfig.MongoPort = mongoport
	}

	if mongoSwitch, ok := serverConfig.GetServConfInt("MONGO", "switch"); ok {
		ftserverConfig.MongoSwitch = mongoSwitch
	}

	if iconPath, ok := serverConfig.GetServConfString("STORAGE", "iconPath"); ok {
		ftserverConfig.IconPath = iconPath
	}

	return nil
}

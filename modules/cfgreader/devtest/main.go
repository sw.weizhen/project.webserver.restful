package main

import (
	"fmt"

	// cfgr "gitlab.com/alexia.shaowei/submodule/cfgreader"
	cfgr "gitlab.com/sw.weizhen/project.webserver.restful/modules/cfgreader"
)

func main() {
	fmt.Println("main() -> init")

	data := websiteCfg{}

	err := cfgr.ReadYAMLConfig("website.yml", &data)
	if err != nil {
		fmt.Printf("read yaml data error: %v\n", err)
		return
	}

	fmt.Println(data)

}

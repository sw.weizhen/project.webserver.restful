package responsedata

const (
	ERROR_CODE_NONE                                 = -1
	ERROR_CODE_FILE_UPLOAD                          = 10
	ERROR_CODE_IMAGE_UPLOAD                         = 11
	ERROR_CODE_BASE64_IMAGE_UPLOAD                  = 12
	ERROR_CODE_DATE_TO_JSON                         = 10000
	ERROR_CODE_JSON_TO_DATA                         = 10001
	ERROR_CODE_DB_SYSTEM                            = 20000
	ERROR_CODE_DB_SYNTAX                            = 20001
	ERROR_CODE_DB_NOUPDATE                          = 20002
	ERROR_CODE_DB_NOINSERTED                        = 20003
	ERROR_CODE_DB_NODELETED                         = 20004
	ERROR_CODE_PARAMETER_EMPTY                      = 30000
	ERROR_CODE_PARAMETER_INVALID                    = 30001
	ERROR_CODE_ACCOUNT_EXIST                        = 40001
	ERROR_CODE_ACCOUNT_NOT_EXIST                    = 40002
	ERROR_CODE_ACCOUNT_ERR_PASSWORD                 = 40003
	ERROR_CODE_ACCOUNT_NOT_ACTIVATED                = 40004
	ERROR_CODE_ACCOUNT_LOGIN_PARAMETER              = 40005
	ERROR_CODE_ACCOUNT_LOGIN_NX_FREQUENCY           = 40006
	ERROR_CODE_ACCOUNT_LOGIN_ACCOUNT_LOCK           = 40007
	ERROR_CODE_ACCOUNT_LOGIN_JWT                    = 40008
	ERROR_CODE_ACCOUNT_LOGIN_IP_DENIED              = 40009
	ERROR_CODE_ACCOUNT_AUTH_PARAM_MISSING           = 40050
	ERROR_CODE_ACCOUNT_AUTH_INVALID                 = 40051
	ERROR_CODE_ACCOUNT_AUTH_CLAIMS                  = 40052
	ERROR_CODE_ACCOUNT_AUTH_TOKEN_REFRESH           = 40053
	ERROR_CODE_ACCOUNT_AUTH_NIL_TOKEN               = 40054
	ERROR_CODE_ACCOUNT_AUTH_NOT_TOKEN               = 40055
	ERROR_CODE_ACCOUNT_AUTH_EXPIRED_TOKEN           = 40056
	ERROR_CODE_ACCOUNT_AUTH_ILLEGAL_TOKEN           = 40057
	ERROR_CODE_ACCOUNT_AUTH_UNKNOWNUSER             = 40058
	ERROR_CODE_ACCOUNT_REGIST_USERNAME_EMPTY        = 40101
	ERROR_CODE_ACCOUNT_REGIST_PASSWORD_EMPTY        = 40102
	ERROR_CODE_ACCOUNT_REGIST_PASSWORD_COMPARE      = 40103
	ERROR_CODE_ACCOUNT_REGIST_CONFIRMPASSWORD_EMPTY = 40104
	ERROR_CODE_ACCOUNT_REGIST_ACCOUNT_EXIST         = 40105
	ERROR_CODE_PERMISSION_DENIED                    = 50000
	ERROR_CODE_PERMISSION_DENIED_PRIVILEGEACT       = 50001
	ERROR_CODE_DECRYPT_FAIL                         = 60000
	ERROR_CODE_HTTP_REQ_PANIC                       = 800000
	ERROR_CODE_UNDEFINED                            = 999999
)

const (
	ERROR_MSG_NONE                                 = ""
	ERROR_MSG_FILE_UPLOAD                          = "file upload failed"
	ERROR_MSG_IMAGE_UPLOAD                         = "image upload failed"
	ERROR_MSG_BASE64_IMAGE_UPLOAD                  = "base64 image upload faild"
	ERROR_MSG_DATA_TO_JSON                         = "transfer JSON failed"
	ERROR_MSG_JSON_TO_DATA                         = "transfer JSON failed"
	ERROR_MSG_DB_SYSTEM                            = "db failed"
	ERROR_MSG_DB_SYNTAX                            = "db failed"
	ERROR_MSG_DB_NOUPDATE                          = "no rows were updated"
	ERROR_MSG_DB_NOINSERTED                        = "no rows were inserted"
	ERROR_MSG_DB_NODELETED                         = "no rows were deleted"
	ERROR_MSG_PARAMETER_EMPTY                      = "request parameter empty"
	ERROR_MSG_PARAMETER_INVALID                    = "one or more parameters are invalid"
	ERROR_MSG_ACCOUNT_EXIST                        = "the account is already existed"
	ERROR_MSG_ACCOUNT_NOT_EXIST                    = "account not exist"
	ERROR_MSG_ACCOUNT_ERR_PASSWORD                 = "password incorrect"
	ERROR_MSG_ACCOUNT_NOT_ACTIVATED                = "account is not activated yet"
	ERROR_MSG_ACCOUNT_LOGIN_PARAMETER              = "login parameter error"
	ERROR_MSG_ACCOUNT_LOGIN_NX_FREQUENCY           = "maximum operating frequency"
	ERROR_MSG_ACCOUNT_LOGIN_ACCOUNT_LOCK           = "account locked"
	ERROR_MSG_ACCOUNT_LOGIN_JWT                    = "fail to generate token "
	ERROR_MSG_ACCOUNT_LOGIN_IP_DENIED              = "login ip denied"
	ERROR_MSG_ACCOUNT_AUTH_PARAM_MISSING           = "missing authentication token"
	ERROR_MSG_ACCOUNT_AUTH_INVALID                 = "invalid token"
	ERROR_MSG_ACCOUNT_AUTH_CLAIMS                  = "fail to convert claims"
	ERROR_MSG_ACCOUNT_AUTH_TOKEN_REFRESH           = "fail to refresh token"
	ERROR_MSG_ACCOUNT_AUTH_NIL_TOKEN               = "fail to create token"
	ERROR_MSG_ACCOUNT_AUTH_NOT_TOKEN               = "not a token"
	ERROR_MSG_ACCOUNT_AUTH_EXPIRED_TOKEN           = "expired token"
	ERROR_MSG_ACCOUNT_AUTH_ILLEGAL_TOKEN           = "illegal token"
	ERROR_MSG_ACCOUNT_AUTH_UNKNOWNUSER             = "unknown user"
	ERROR_MSG_ACCOUNT_REGIST_USERNAME_EMPTY        = "empty username"
	ERROR_MSG_ACCOUNT_REGIST_PASSWORD_EMPTY        = "empty password"
	ERROR_MSG_ACCOUNT_REGIST_PASSWORD_COMPARE      = "password and confirm password does not match"
	ERROR_MSG_ACCOUNT_REGIST_CONFIRMPASSWORD_EMPTY = "empty confirm password"
	ERROR_MSG_PERMISSION_DENIED                    = "permission denied"
	ERROR_MSG_PERMISSION_DENIED_PRIVILEGEACT       = "permission denied(privilege)"
	ERROR_MSG_DECRYPT_FAIL                         = "decryption failed "
	ERROR_MSG_HTTP_REQ_PANIC                       = "http request fatal error"
	ERROR_MSG_UNDEFINED                            = "undifined system error"
)

const (
	RESPONSE_CODE_SUCCESS = true
	RESPONSE_CODE_FAILURE = false
)

package responsedata

import (
	"bytes"
	"encoding/json"
)

func codeResponseJSONError() []byte {
	var respData RespDataABS = RespDataABS{
		Success: false,
		ErrCode: ERROR_CODE_DATE_TO_JSON,
		ErrMsg:  ERROR_MSG_DATA_TO_JSON,
	}

	rtnJSON, _ := json.Marshal(respData)

	return []byte(rtnJSON)
}

// CodeResponseAbstraction ...
func CodeResponseAbstraction(success bool, errCode int, errMsg string, rtnData interface{}, setEscpHTML bool) []byte {
	var respData RespDataABS = RespDataABS{
		ErrCode:      errCode,
		ErrMsg:       errMsg,
		Success:      success,
		ResponseData: rtnData,
	}

	byteBuf := bytes.NewBuffer([]byte{})
	encoder := json.NewEncoder(byteBuf)
	encoder.SetEscapeHTML(setEscpHTML)
	err := encoder.Encode(respData)
	if err != nil {
		return codeResponseJSONError()
	}

	return byteBuf.Bytes()
}

// CodeResponseSuccess ...
func CodeResponseSuccess(respData interface{}) []byte {
	return CodeResponseAbstraction(RESPONSE_CODE_SUCCESS, ERROR_CODE_NONE, ERROR_MSG_NONE, respData, true)
}

func CodeResponseSuccessWithoutEscapeHTML(respData interface{}) []byte {
	return CodeResponseAbstraction(RESPONSE_CODE_SUCCESS, ERROR_CODE_NONE, ERROR_MSG_NONE, respData, false)
}

// CodeResponseFailure ...
func CodeResponseFailure(errCode int, errMsg string) []byte {
	return CodeResponseAbstraction(RESPONSE_CODE_FAILURE, errCode, errMsg, nil, true)
}

// CodeResponseFailureWithMessage ...
func CodeResponseFailureWithMessage(msg string, errCode int, errMsg string) []byte {
	return CodeResponseAbstraction(RESPONSE_CODE_FAILURE, errCode, errMsg, msg, true)
}

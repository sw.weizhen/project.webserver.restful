package gojwt

import (
	"encoding/json"
	"strings"
)

type JWToken struct {
	Header       map[string]interface{}
	SignatureOpe SignatureOperate
	Claims       Claims
	Token        string
	Signature    string
	Valid        bool
}

func (ref *JWToken) Sign(key interface{}) (string, error) {

	var err error
	var sign string
	var signVal string

	if signVal, err = ref.signing(); err != nil {
		return EMPTY, err
	}

	if sign, err = ref.SignatureOpe.Sign(signVal, key); err != nil {
		return EMPTY, err
	}

	return strings.Join([]string{signVal, sign}, DOT), nil
}

func (ref *JWToken) signing() (string, error) {
	var err error
	pts := make([]string, 2)

	for i := range pts {
		var jVal []byte

		if i == 0 {
			if jVal, err = json.Marshal(ref.Header); err != nil {
				return EMPTY, err
			}

		} else {
			if jVal, err = json.Marshal(ref.Claims); err != nil {
				return EMPTY, err
			}

		}

		pts[i] = encodeSignatureBase64(jVal)
	}

	return strings.Join(pts, DOT), nil
}

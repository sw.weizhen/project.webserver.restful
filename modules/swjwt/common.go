package gojwt

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"strings"
)

func expired(expAt, now int64, required bool) bool {
	if expAt == 0 {
		return !required
	}

	return now <= expAt
}

func issuedAt(isuAt, now int64, required bool) bool {
	if isuAt == 0 {
		return !required
	}

	return now >= isuAt
}

func notBefore(nBefore, now int64, required bool) bool {
	if nBefore == 0 {
		return !required
	}

	return now >= nBefore
}

func encodeSignatureBase64(seg []byte) string {
	return strings.TrimRight(base64.URLEncoding.EncodeToString(seg), EQUAL)
}

func decodeSignatureBase64(seg string) ([]byte, error) {
	if l := len(seg) % SEGMENT; l > 0 {
		seg += strings.Repeat(EQUAL, SEGMENT-l)
	}

	return base64.URLEncoding.DecodeString(seg)
}

func parseJWT(input string, claims Claims) (token *JWToken, parts []string, err error) {
	parts = strings.Split(input, DOT)
	if len(parts) != PARTS {
		return nil, parts, newErrWhenValidating(ERR_TOKEN_PARTS_MISMATCH, ERR_VALIDATION_MALFORMED)
	}

	token = &JWToken{
		Token: input,
	}

	var btHeader []byte
	if btHeader, err = decodeSignatureBase64(parts[0]); err != nil {
		if strings.HasPrefix(strings.ToLower(input), BEARER) {
			return token, parts, newErrWhenValidating(ERR_TOKEN_CONTAINS_BEARER, ERR_VALIDATION_MALFORMED)
		}

		return token, parts, &ErrorValidation{
			ErrStores: err,
			Errors:    ERR_VALIDATION_MALFORMED,
		}
	}

	if err = json.Unmarshal(btHeader, &token.Header); err != nil {
		return token, parts, &ErrorValidation{
			ErrStores: err,
			Errors:    ERR_VALIDATION_MALFORMED,
		}
	}

	var btClaims []byte
	token.Claims = claims
	if btClaims, err = decodeSignatureBase64(parts[1]); err != nil {
		return token, parts, &ErrorValidation{
			ErrStores: err,
			Errors:    ERR_VALIDATION_MALFORMED,
		}
	}

	decoder := json.NewDecoder(bytes.NewBuffer(btClaims))
	if v, ok := token.Claims.(MapClaims); ok {
		err = decoder.Decode(&v)

	} else {
		err = decoder.Decode(&claims)
	}

	if err != nil {
		return token, parts, &ErrorValidation{
			ErrStores: err,
			Errors:    ERR_VALIDATION_MALFORMED,
		}
	}

	if v, ok := token.Header[TAG_ALG].(string); ok {
		if token.SignatureOpe = getSignatureOpeMap(v); token.SignatureOpe == nil {

			//it can't be
			return token, parts, newErrWhenValidating(ERR_UNAVAILABLE_SIGN_OPE, ERR_VALIDATION_UNVERIFIABLE)
		}

	} else {
		return token, parts, newErrWhenValidating(ERR_UNIMPLEMENTED_SIGN_OPE, ERR_VALIDATION_UNVERIFIABLE)
	}

	return token, parts, nil
}

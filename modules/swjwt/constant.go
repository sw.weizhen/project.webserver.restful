package gojwt

const (
	PARTS   = 3
	SEGMENT = 4
)

const (
	DOT   = "."
	EQUAL = "="
	EMPTY = ""
)

const (
	HS256 = "HS256"
	HS384 = "HS384"
	HS512 = "HS512"
)

const (
	BEARER  = "bearer "
	TAG_EXP = "exp"
	TAG_IAT = "iat"
	TAG_NBF = "nbf"
	TAG_ALG = "alg"
)

const (
	ERR_HASH_UNAVAILABLE         = "unavailable hash function"
	ERR_INVALID_TOKEN            = "invalid token"
	ERR_INVALID_KEY              = "invalid shKey"
	ERR_INVALID_KEY_TYPE         = "invalid shkey type"
	ERR_INVALID_SIGNATURE        = "invalid signature"
	ERR_INVALID_PEM_KEY          = "invalid Key: Key must be a PEM encoded PKCS1 or PKCS8 key"
	ERR_INVALID_PEM_PUBLIC_KEY   = "invalid RSA public key"
	ERR_TOKEN_EXPIRED_BY         = "token expired by %v"
	ERR_TOKEN_EXPIRED            = "token expired"
	ERR_TOKEN_NOT_VALID_YET      = "token not valid yet"
	ERR_TOKEN_USED_BEFORE_ISSUED = "token used before issued"
	ERR_TOKEN_CONTAINS_BEARER    = "token should not contain 'bearer '"
	ERR_TOKEN_PARTS_MISMATCH     = "token number of parts mismatch"
	ERR_UNAVAILABLE_SIGN_OPE     = "unavailable signing crypto"
	ERR_UNIMPLEMENTED_SIGN_OPE   = "unimplemented signing crypto"
	ERR_MISS_SIGNFUNC            = "miss sign func"
)

const (
	ERR_VALIDATION_MALFORMED uint32 = 1 << iota
	ERR_VALIDATION_UNVERIFIABLE
	ERR_VALIDATION_SIGNATURE_INVALID
	ERR_VALIDATION_AUDIENCE
	ERR_VALIDATION_EXPIRED
	ERR_VALIDATION_ISSUED_AT
	ERR_VALIDATION_ISSUER
	ERR_VALIDATION_NOT_VALID_YET
	ERR_VALIDATION_ID
	ERR_VALIDATION_CLAIMS_INVALID
)

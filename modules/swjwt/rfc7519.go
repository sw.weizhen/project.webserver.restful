package gojwt

type SignatureOperate interface {
	Verify(sign, signature string, key interface{}) error
	Sign(sign string, key interface{}) (string, error)
	Crypto() string
}

type Claims interface {
	Validating() error
}

// RFC7519 - 4.1 Registered Claim Names
type RegisteredClaimNames struct {
	JWTID          string `json:"jti,omitempty"`
	Subject        string `json:"sub,omitempty"`
	Audience       string `json:"aud,omitempty"`
	Issuer         string `json:"iss,omitempty"`
	IssuedAt       int64  `json:"iat,omitempty"`
	NotBefore      int64  `json:"nbf,omitempty"`
	ExpirationTime int64  `json:"exp,omitempty"`
}

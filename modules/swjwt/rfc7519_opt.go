package gojwt

import (
	"errors"
	"fmt"
	"time"
)

var TimeNow = time.Now

func New(signOpe SignatureOperate, claims Claims) *JWToken {
	return &JWToken{
		Claims:       claims,
		SignatureOpe: signOpe,

		Header: map[string]interface{}{
			"typ": "JWT",
			"alg": signOpe.Crypto(),
		},
	}
}

func (ref RegisteredClaimNames) Validating() error {
	now := TimeNow().Unix()
	err := new(ErrorValidation)

	if !ref.VerifyExpiresTime(now, false) {
		tDiff := time.Unix(now, 0).Sub(time.Unix(ref.ExpirationTime, 0))
		err.ErrStores = fmt.Errorf(ERR_TOKEN_EXPIRED_BY, tDiff)
		err.Errors |= ERR_VALIDATION_EXPIRED
	}

	if !ref.VerifyIssuedAt(now, false) {
		err.ErrStores = errors.New(ERR_TOKEN_USED_BEFORE_ISSUED)
		err.Errors |= ERR_VALIDATION_ISSUED_AT
	}

	if !ref.VerifyNotBefore(now, false) {
		err.ErrStores = errors.New(ERR_TOKEN_NOT_VALID_YET)
		err.Errors |= ERR_VALIDATION_NOT_VALID_YET
	}

	if err.isValid() {
		return nil
	}

	return err
}

func (ref *RegisteredClaimNames) VerifyExpiresTime(now int64, required bool) bool {
	return expired(ref.ExpirationTime, now, required)
}

func (ref *RegisteredClaimNames) VerifyIssuedAt(now int64, required bool) bool {
	return issuedAt(ref.IssuedAt, now, required)
}

func (ref *RegisteredClaimNames) VerifyNotBefore(now int64, required bool) bool {
	return notBefore(ref.NotBefore, now, required)
}

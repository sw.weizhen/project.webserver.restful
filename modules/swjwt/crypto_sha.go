package gojwt

import (
	"crypto"
	"crypto/hmac"
	_ "crypto/x509"
	"errors"
)

func init() {

	SigningHS256 = &SignatureSHA{HS256, crypto.SHA256}
	putSignatureOpeMap(SigningHS256.Crypto(), func() SignatureOperate {
		return SigningHS256
	})

	SigningHS384 = &SignatureSHA{HS384, crypto.SHA384}
	putSignatureOpeMap(SigningHS384.Crypto(), func() SignatureOperate {
		return SigningHS384
	})

	SigningHS512 = &SignatureSHA{HS512, crypto.SHA512}
	putSignatureOpeMap(SigningHS512.Crypto(), func() SignatureOperate {
		return SigningHS512
	})
}

type SignatureSHA struct {
	SHA  string
	Hash crypto.Hash
}

var SigningHS256 *SignatureSHA
var SigningHS384 *SignatureSHA
var SigningHS512 *SignatureSHA

func (ref *SignatureSHA) Crypto() string {
	return ref.SHA
}

func (ref *SignatureSHA) Sign(signature string, key interface{}) (string, error) {
	if btKey, ok := key.([]byte); ok {
		if !ref.Hash.Available() {
			return EMPTY, errors.New(ERR_HASH_UNAVAILABLE)
		}

		h := hmac.New(ref.Hash.New, btKey)
		h.Write([]byte(signature))

		return encodeSignatureBase64(h.Sum(nil)), nil
	}

	return EMPTY, errors.New(ERR_INVALID_KEY_TYPE)
}

func (m *SignatureSHA) Verify(sign, signature string, key interface{}) error {

	btKey, ok := key.([]byte)
	if !ok {
		return errors.New(ERR_INVALID_KEY_TYPE)
	}

	sgn, err := decodeSignatureBase64(signature)
	if err != nil {
		return err
	}

	if !m.Hash.Available() {
		return errors.New(ERR_HASH_UNAVAILABLE)
	}

	h := hmac.New(m.Hash.New, btKey)
	h.Write([]byte(sign))
	if !hmac.Equal(sgn, h.Sum(nil)) {
		return errors.New(ERR_INVALID_SIGNATURE)
	}

	return nil
}

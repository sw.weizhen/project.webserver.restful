package gojwt

import "strings"

type signFunc func(*JWToken) (interface{}, error)

func Decrypt(input string, claims Claims, signfunc signFunc) (*JWToken, error) {
	token, parts, err := parseJWT(input, claims)
	if err != nil {
		return token, nil
	}

	var sF interface{}
	if signfunc == nil {
		return token, newErrWhenValidating(ERR_MISS_SIGNFUNC, ERR_VALIDATION_UNVERIFIABLE)
	}

	if sF, err = signfunc(token); err != nil {
		if v, ok := err.(*ErrorValidation); ok {
			return token, v
		}

		return token, &ErrorValidation{
			ErrStores: err,
			Errors:    ERR_VALIDATION_UNVERIFIABLE,
		}
	}

	stErr := &ErrorValidation{}

	if err := token.Claims.Validating(); err != nil {
		if v, ok := err.(*ErrorValidation); !ok {
			stErr = &ErrorValidation{
				ErrStores: err,
				Errors:    ERR_VALIDATION_CLAIMS_INVALID,
			}

		} else {
			stErr = v
		}
	}

	token.Signature = parts[2]
	if err = token.SignatureOpe.Verify(strings.Join(parts[0:2], DOT), token.Signature, sF); err != nil {
		stErr.ErrStores = err
		stErr.Errors |= ERR_VALIDATION_SIGNATURE_INVALID
	}

	if stErr.isValid() {
		token.Valid = true
		return token, nil
	}

	return token, stErr
}

package gojwt

func newErrWhenValidating(errMsg string, errs uint32) *ErrorValidation {
	return &ErrorValidation{
		ErrMsg: errMsg,
		Errors: errs,
	}
}

type ErrorValidation struct {
	ErrStores error
	Errors    uint32
	ErrMsg    string
}

func (ref ErrorValidation) Error() string {
	if ref.ErrStores != nil {
		return ref.ErrStores.Error()

	} else if ref.ErrMsg != EMPTY {
		return ref.ErrMsg

	} else {
		return ERR_INVALID_TOKEN

	}
}

func (ref *ErrorValidation) isValid() bool {
	return ref.Errors == 0
}

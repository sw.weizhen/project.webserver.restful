package userinfo

import (
	jwt "gitlab.com/sw.weizhen/project.webserver.restful/modules/swjwt"
)

type UserInfo struct {
	ID             int
	UID            int
	ChannelID      int
	Name           string
	NickName       string
	RoleID         int
	UserType       UserTypeEnum
	UserStatus     int
	Timezone       int
	IsDelete       bool
	IsForbidden    bool
	LastLoginTime  int
	CreateUserName string
	CreateDate     int

	jwt.RegisteredClaimNames
}

package userinfo

type UserTypeEnum int

const (
	ENUM_TYPE_USER       UserTypeEnum = 0
	ENUM_TYPE_SUBACCOUNT UserTypeEnum = 1
	ENUM_TYPE_AGENT      UserTypeEnum = 2
)

func (ref UserTypeEnum) Index() int {
	return int(ref)
}

func (ref UserTypeEnum) String() string {
	switch ref {
	case ENUM_TYPE_USER:
		return "User"
	case ENUM_TYPE_SUBACCOUNT:
		return "SubAccount"
	case ENUM_TYPE_AGENT:
		return "Agent"
	default:
		return "Undefined"
	}
}

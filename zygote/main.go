package main

import (
	"gitlab.com/sw.weizhen/project.webserver.restful/server"
	_ "gitlab.com/sw.weizhen/project.webserver.restful/zygote/controller"
)

func main() {

	server.Deploy().Run()
}

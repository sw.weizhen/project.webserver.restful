package controller

import (
	reg "gitlab.com/sw.weizhen/project.webserver.restful/registrar"
	api "gitlab.com/sw.weizhen/project.webserver.restful/zygote/service/jwt"
)

func init() {

	reg.Controller.AddRouter(reg.GET, "/jwt", api.Auth, reg.Option{VerifyAuth: true})
	reg.Controller.AddRouter(reg.POST, "/jwt", api.CreateJWT)
	reg.Controller.AddRouter(reg.GET, "/white/list", api.WhiteList)
	reg.Controller.AddRouter(reg.GET, "/ServInfo", api.ServInfo)

	reg.Controller.AddRouter(reg.GET, "/ServInfo", api.ServInfoOpts, reg.Option{VerifyAuth: true, VerifyPrivilegeActionCode: "xxx"})
}

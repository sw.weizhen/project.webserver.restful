package jwt

import (
	"fmt"

	r "gitlab.com/sw.weizhen/project.webserver.restful/modules/responsedata"
	u "gitlab.com/sw.weizhen/project.webserver.restful/modules/userinfo"
	"gitlab.com/sw.weizhen/project.webserver.restful/openvar"
	"gitlab.com/sw.weizhen/project.webserver.restful/registrar"
	serv "gitlab.com/sw.weizhen/project.webserver.restful/server"
)

func CreateJWT(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {

	// login ... coding here

	user := u.UserInfo{
		ID:          1535,
		UID:         56418,
		ChannelID:   5,
		Name:        "ChihHsi",
		NickName:    "Nii",
		IsDelete:    true,
		IsForbidden: false,
		UserType:    u.ENUM_TYPE_SUBACCOUNT,
	}

	token, err := serv.NewJWToken(user)
	if err != nil {
		openvar.Log.Error(err.Error())

		return r.CodeResponseFailure(r.ERROR_CODE_ACCOUNT_LOGIN_JWT, r.ERROR_MSG_ACCOUNT_LOGIN_JWT)
	}

	mp := make(map[string]interface{})
	mp["jwt"] = token
	mp["user"] = user

	return r.CodeResponseSuccess(mp)
}

func Auth(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {
	return r.CodeResponseSuccessWithoutEscapeHTML(fmt.Sprintf("%+v", servInfo.User))
}

func WhiteList(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {

	var wls map[string][]string = map[string][]string{
		"white list": openvar.WhiteList,
	}

	return r.CodeResponseSuccess(wls)
}

func ServInfo(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {
	fmt.Println("ServInfo")
	return r.CodeResponseSuccess(fmt.Sprintf("%+v", servInfo))
}

func ServInfoOpts(paramReq *registrar.ParamReq, servInfo *registrar.ServInfo) []byte {
	fmt.Println("ServInfoOpts")
	return r.CodeResponseSuccessWithoutEscapeHTML(fmt.Sprintf("%+v", servInfo))
}
